from functools import wraps
from typing import Dict, Any, Callable

import firebase_admin.auth
from logbook import Logger
from flask import Request
from http_constants.headers import HttpHeaders

from .cloudfunc import GCHttpStatus, firebase_abort

logger = Logger(__name__)


# Ref: https://code.luasoftware.com/tutorials/google-cloud-functions/create-compatitle-cloud-function-for-firebase-with-python/  # noqa
def firebase_auth_required(f: Callable[Request, ...]):
    @wraps(f)
    def wrapper(request: Request):
        auth_value = request.headers.get(HttpHeaders.AUTHORIZATION)   # type: str
        # Ref: https://github.com/firebase/firebase-admin-node/blob/master/test/integration/auth.spec.ts#L375
        if not auth_value or not auth_value.startswith('Bearer '):
            return firebase_abort(GCHttpStatus.UNAUTHENTICATED, 'wrong-authorization-header')
        fb_token = auth_value.split()[1]
        logger.debug('Firebase ID token: {}', fb_token)
        try:
            decoded_token = firebase_admin.auth.verify_id_token(fb_token)
            # Something like:
            # {
            #     "iss": "https://securetoken.google.com/sunshine-super-app",
            #     "aud": "sunshine-super-app",
            #     "auth_time": 1582022856,
            #     "user_id": "VOdj1kzLO5N2s7UaRoxJEdGYBBP2",
            #     "sub": "VOdj1kzLO5N2s7UaRoxJEdGYBBP2",
            #     "iat": 1582022856,
            #     "exp": 1582026456,
            #     "email": "quannh@sunshinegroup.vn",
            #     "email_verified": False,
            #     "firebase": {
            #         "identities": {"email": ["quannh@sunshinegroup.vn"]},
            #         "sign_in_provider": "custom",
            #     },
            #     "uid": "VOdj1kzLO5N2s7UaRoxJEdGYBBP2",
            # }
        except ValueError as e:
            logger.error('The supplied token is not a Firebase ID token. Error: {}', e)
            firebase_abort(GCHttpStatus.UNAUTHENTICATED, message='Invalid Firebase ID token')
        except firebase_admin.auth.InvalidIdTokenError as e:
            logger.error('Firebase message: {}', e)
            firebase_abort(GCHttpStatus.UNAUTHENTICATED, message=str(e))
        logger.debug('Decoded JWT: {}', decoded_token)
        return f(request, decoded_token)
    return wrapper


def https_callable_compliant(f: Callable[Request, ...]):
    @wraps(f)
    def wrapper(request: Request, decoded_token: Dict[str, Any]):
        if not request.is_json:
            firebase_abort(GCHttpStatus.INVALID_ARGUMENT, 'not-json')
        try:
            request_data = request.get_json()['data']
        except KeyError:
            logger.error('Missing "data" field')
            firebase_abort(GCHttpStatus.INVALID_ARGUMENT, 'missing-data', message='Missing "data" field')
        return f(request, request_data, decoded_token)
    return wrapper
