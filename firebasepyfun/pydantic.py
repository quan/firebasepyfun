import sys
from decimal import Decimal
from datetime import datetime, timedelta
from typing import Union

from pendulum import DateTime, Duration
from pydantic import BaseModel as _BaseModel
from google.cloud.firestore import SERVER_TIMESTAMP, DocumentReference
from google.api_core.datetime_helpers import DatetimeWithNanoseconds

if sys.version_info[:2] < (3, 8):
    from typing_extensions import Literal
else:
    from typing import Literal


FireDateTime = Union[datetime, DateTime, Literal[SERVER_TIMESTAMP]]


class FireDocReference(DocumentReference):
    '''Pydantic field for Google Firestore's Reference field type'''
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if not isinstance(v, DocumentReference):
            msg = f'Firestore DocumentReference required. Got {type(v)}'
            raise TypeError(msg)
        return v


# Extend Pydantic BaseModel, allowing to export to a dict comprising of standard data types, so that it can be
# serialized by any JSON encoder
class BaseModel(_BaseModel):
    class Config:
        # datetime field returned by Google Firestore has custom type: DatetimeWithNanoseconds,
        # which, pydantic doesn't know how serialize to JSON. So we need to guide pydantic how to do.
        json_encoders = {
            DatetimeWithNanoseconds: lambda v: v.rfc3339(),
            Duration: lambda v: f'{v.in_hours()}:{v.minutes}:{v.remaining_seconds}',
            DateTime: lambda v: v.to_iso8601_string(),
        }

    def to_norm_dict(self, by_alias=False):
        '''Produce dict data with only Python standard data types,
        which can be consumed later by Google Cloud client library and some JSON encoder.'''
        data = self.dict(by_alias=by_alias)
        for k, field in self.__fields__.items():
            key = k if not by_alias else field.alias
            value = data[key]
            if value is None:
                continue
            # Google Firestore client cannot send out Decimal
            if isinstance(value, Decimal):
                data[k] = float(value)
                continue
            ftype = field.type_
            if ftype is Duration:
                data[k] = timedelta(days=value.days, seconds=value.seconds)
            elif ftype is DatetimeWithNanoseconds or ftype is DateTime:
                data[k] = datetime(value.year, value.month, value.day,
                                   value.hour, value.minute, value.second, value.microsecond,
                                   tzinfo=value.tzinfo)
        return data
