from enum import IntEnum
from typing import Optional, Dict, Any

from logbook import Logger
from pydantic import ValidationError
from http_constants.headers import HttpHeaders
from flask import Response, jsonify, abort

from .compat import json_dumps


ErrorDetail = Optional[Dict[str, str]]
logger = Logger(__name__)


# Ref: https://cloud.google.com/apis/design/errors#http_mapping
class GCHttpStatus(IntEnum):
    OK = 200
    INVALID_ARGUMENT = 400
    # TODO: Due to Enum behavior, we cannot send the alias name of a enum member
    # (we cannot send "FAILED_PRECONDITION" string)
    FAILED_PRECONDITION = 400
    OUT_OF_RANGE = 400
    UNAUTHENTICATED = 401
    PERMISSION_DENIED = 403
    NOT_FOUND = 404
    ALREADY_EXISTS = 409
    ABORTED = 409
    RESOURCE_EXHAUSTED = 429
    CANCELLED = 499
    INTERNAL = 500
    DATA_LOSS = 500
    UNKNOWN = 500
    NOT_IMPLEMENTED = 501
    UNAVAILABLE = 503
    DEADLINE_EXCEEDED = 504


# Ref: https://firebase.google.com/docs/functions/callable-reference
def firebase_abort(status: GCHttpStatus, message: Optional[str] = None, details: ErrorDetail = None):
    data = {
        'error': {
            'status': status.name,
            'message': message
        }
    }
    if details:
        data['error']['details'] = details
    response = jsonify(data)
    response.status_code = status.value
    abort(response)


def error_detail_from_pydantic(error: ValidationError) -> Dict[str, str]:
    '''
    Build dict of validation errors from pydantic's ValidationError.

    The key of this dict is the name of field which didn't pass validation.
    They value of this dict is the message, explaning what is wrong.
    '''
    details = {}
    for d in error.errors():
        field_layers = (str(n) for n in d['loc'])
        field = '.'.join(field_layers)
        details[field] = d['msg']
    return details


def make_json_response(data: Dict[str, Any]) -> Response:
    '''
    Mimics Flask's jsonify, but use some high-performance JSON encoders, and display datetime in ISO string.

    Note that its argument is not as flexible (and confusing) as Flask's jsonify.
    '''
    serialized = json_dumps(data)
    return Response(f'{serialized}\n', mimetype=HttpHeaders.CONTENT_TYPE_VALUES.json)


def firebase_response(data: Dict[str, Any]) -> Response:
    '''
    Make Flask Response from dict of data, rearrange fields to match Firebase Http Callable.
    '''
    wrapped = {'data': data}
    return make_json_response(wrapped)
