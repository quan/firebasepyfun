import json
from functools import partial

json_dumps = partial(json.dumps, ensure_ascii=False)

try:
    import rapidjson
    from rapidjson import NM_DECIMAL, DM_ISO8601, UM_CANONICAL
    json_dumps = partial(rapidjson.dumps, ensure_ascii=False, number_mode=NM_DECIMAL,
                         datetime_mode=DM_ISO8601, uuid_mode=UM_CANONICAL)
except ImportError:
    try:
        import orjson
        from orjson import OPT_SERIALIZE_DATACLASS, OPT_SERIALIZE_UUID
        opt = OPT_SERIALIZE_DATACLASS | OPT_SERIALIZE_UUID

        # orjson returns bytes, not str, so we have to decode
        def _dumps(data, option):
            return orjson.dumps(data, option=option).decode()
        json_dumps = partial(_dumps, option=opt)
    except ImportError:
        pass
