import requests


URL_VERIFY_CUSTOM_TOKEN = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken'


def verify_custom_token(custom_token: str, firebase_api_key: str) -> str:
    '''Verify Firebase custom token and return ID token'''
    # Ref: https://github.com/firebase/firebase-admin-python/blob/master/integration/test_auth.py#L41
    body = {'token': custom_token, 'returnSecureToken': True}
    params = {'key': firebase_api_key}
    resp = requests.post(URL_VERIFY_CUSTOM_TOKEN, params=params, json=body)
    resp.raise_for_status()
    return resp.json().get('idToken')
